import models.Rectangle;
import models.Circle;

public class App {

    public static void main(String[] args) throws Exception {

        Circle circle = new Circle(15);

        Rectangle rectangle = new Rectangle(10, 20);

        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println("Circle");
        System.out.println("Chu vi" + " " + circle.getPerimeter());
        System.out.println("Ban Kinh" + " " + circle.getArea());
        System.out.println("Rectangle");
        System.out.println("Chu vi" + " " + rectangle.getPerimeter());
        System.out.println("Dien tich" + " " + rectangle.getArea());
    }
}
