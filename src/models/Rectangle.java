package models;

import interfaces.GeometricObject;

public class Rectangle implements GeometricObject{
    private double wdith;
    private double length;

    public Rectangle(double wdith, double length) {
        this.wdith = wdith;
        this.length = length;
    }

    @Override
    public String toString() {
        return "Rectangle [wdith=" + wdith + ", length=" + length + "]";
    }

    public double getArea(){
        return length * wdith;
    }
    @Override
    public double getPerimeter() {
        return 2 * (length + wdith);
    }
}
